require 'test_helper'

class SalesmanagersControllerTest < ActionController::TestCase
  setup do
    @salesmanager = salesmanagers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:salesmanagers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create salesmanager" do
    assert_difference('Salesmanager.count') do
      post :create, salesmanager: { name: @salesmanager.name, phone: @salesmanager.phone }
    end

    assert_redirected_to salesmanager_path(assigns(:salesmanager))
  end

  test "should show salesmanager" do
    get :show, id: @salesmanager
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @salesmanager
    assert_response :success
  end

  test "should update salesmanager" do
    patch :update, id: @salesmanager, salesmanager: { name: @salesmanager.name, phone: @salesmanager.phone }
    assert_redirected_to salesmanager_path(assigns(:salesmanager))
  end

  test "should destroy salesmanager" do
    assert_difference('Salesmanager.count', -1) do
      delete :destroy, id: @salesmanager
    end

    assert_redirected_to salesmanagers_path
  end
end
