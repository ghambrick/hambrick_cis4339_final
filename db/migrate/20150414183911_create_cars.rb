class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :year
      t.string :make
      t.string :model
      t.string :color
      t.string :vin
      t.decimal :cost, precision: 8, scale: 2
      t.boolean :sold

      t.timestamps null: false
    end
  end
end
