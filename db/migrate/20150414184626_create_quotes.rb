class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.string :quote_number
      t.integer :salesperson_id
      t.integer :car_id
      t.integer :customer_id
      t.decimal :price, precision: 8, scale: 2
      t.date :date_created
      t.date :date_sold

      t.timestamps null: false
    end
  end
end
