class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.string :loan_number
      t.integer :quote_id
      t.decimal :purprice, precision: 8, scale: 2
      t.decimal :downpayment, precision: 8, scale: 2
      t.integer :term
      t.decimal :tax, precision: 4, scale: 2
      t.decimal :rate, precision: 4, scale: 2
      t.decimal :principle, precision: 8, scale: 2

      t.timestamps null: false
    end
  end
end
