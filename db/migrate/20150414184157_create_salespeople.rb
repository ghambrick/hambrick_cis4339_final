class CreateSalespeople < ActiveRecord::Migration
  def change
    create_table :salespeople do |t|
      t.string :name
      t.string :phone
      t.integer :salesmanager_id

      t.timestamps null: false
    end
  end
end
