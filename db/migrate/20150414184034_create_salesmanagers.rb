class CreateSalesmanagers < ActiveRecord::Migration
  def change
    create_table :salesmanagers do |t|
      t.string :name
      t.string :phone

      t.timestamps null: false
    end
  end
end
