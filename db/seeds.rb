# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# CREATE 2 SALES MANAGERS
  salesmanagers  = Salesmanager.create([
{ name: 'Harry Dalton', phone: '111-111-1111'},
{ name: 'Barry White', phone:'111-111-1112'}
])

# CREATE 4 SALES PERSONS
salepeople = Salesperson.create ([
{ name: 'Toby Maguire', phone: '222-222-2222', salesmanager_id:'1'},
{  name: 'Ted Samuel', phone: '222-222-2223', salesmanager_id:'1'},
{ name: 'Nancy Grace', phone: '222-222-2224', salesmanager_id:'2'},
{  name: 'Betty Rubble', phone: '222-222-2225', salesmanager_id:'2'}
])

# CREATE 12 CARS
cars = Car.create ([
{ year: '1990', make: 'Dodge', model: 'Colt', color: 'Red', vin:'DC1234567', cost: '13500.56', sold: 'false'},
{ year: '1990', make: 'Dodge', model: 'Dakota', color: 'Blue', vin:'DD3456789', cost: '17500.99', sold: 'true'},
{ year: '2000', make: 'Honda', model: 'Accord', color: 'Silver', vin:'HA1234567', cost: '23500.17', sold: 'false'},
{ year: '2001', make: 'Honda', model: 'Accord', color: 'Black', vin:'HA1234567', cost: '19800.12', sold: 'false'},
{ year: '2005', make: 'Honda', model: 'Civic', color: 'White', vin:'HC3333124', cost: '15750.26', sold: 'false'},
{ year: '1963', make: 'Chevrolet', model: 'Corvette', color: 'Red', vin:'CC1111223', cost: '33500.00', sold: 'false'},
{ year: '1968', make: 'Chevrolet', model: 'Impala SS', color: 'Green', vin:'CI6789502', cost: '23500.16', sold: 'false'},
{ year: '1990', make: 'Jaguar', model: 'XKE', color: 'British Racing Green', vin:'JX0099665', cost: '43199.76', sold: 'false'},
{ year: '2013', make: 'BMW', model: '335i', color: 'Blue', vin:'BMW000044', cost: '53500.19', sold: 'false'},
{ year: '2000', make: 'BMW', model: '535i', color: 'Black', vin:'BMW340025', cost: '75500.44', sold: 'false'},
{ year: '2012', make: 'Ford', model: 'Mustang', color: 'Black', vin:'FM1122334', cost: '37900.00', sold: 'false'},
{ year: '2010', make: 'Lexus', model: 'I350', color: 'Red', vin:'LI1234567', cost: '43500.00', sold: 'false'}
])

# CREATE 6 Customers
customers = Customer.create ([
{ name: 'Bobby McGee', phone: '333-333-3333', salesperson_id:'2'},
{ name: 'Roger Waters', phone: '333-333-3334', salesperson_id: '2'},
{ name: 'David Stills', phone: '333-333-3335', salesperson_id: '4'},
{ name: 'Steven Crosby', phone: '333-333-3336', salesperson_id: '4'},
{ name: 'Klaus Voormann', phone: '333-333-3337', salesperson_id: '3'},
{ name: 'Ziggy Stardust', phone: '333-333-3338', salesperson_id: '1'}
])

# CREATE 4 QUOTES
quotes = Quote.create ([
{ quote_number: 'TS001', salesperson_id: '2', car_id: '1', customer_id: '1', price: '14850.62', date_created: '4-4-2015', date_sold: ''},
{ quote_number: 'TS002', salesperson_id: '2', car_id: '2', customer_id: '2', price: '19251.09', date_created: '4-8-2015', date_sold: '4-12-2015'},
{ quote_number: 'BR001', salesperson_id: '4', car_id: '3', customer_id: '3', price: '25850.19', date_created: '4-10-2015', date_sold: ''},
{ quote_number: 'BR002', salesperson_id: '4', car_id: '4', customer_id: '4', price: '21780.13', date_created: '4-12-2015', date_sold: ''}
])