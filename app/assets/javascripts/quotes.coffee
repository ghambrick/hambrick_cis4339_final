# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

updatePrice = ->
  selection_id = $('#quote_car_id').val()
  $.getJSON '/cars/' + selection_id + '/cost', {},(json, response) ->
    $('#quote_price').val( json['cost']*1.1)

    updateSold = ->
  selection_id = $('#quote_car_id').val()
  $.getJSON '/cars/' + selection_id + '/cost', {},(json, response) ->
    $('#quote_price').val( json['cost']*1.1)

$ ->

  $('#quote_car_id').change  -> updatePrice()

  $('#quote_date_sold').change -> updateSold()