json.array!(@cars) do |car|
  json.extract! car, :id, :year, :make, :model, :color, :vin, :cost, :sold
  json.url car_url(car, format: :json)
end
