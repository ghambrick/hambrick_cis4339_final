json.extract! @car, :id, :year, :make, :model, :color, :vin, :cost, :sold, :created_at, :updated_at
