json.array!(@quotes) do |quote|
  json.extract! quote, :id, :quote_number, :salespaerson_id, :car_id, :customer_id, :price, :date_created, :date_sold
  json.url quote_url(quote, format: :json)
end
