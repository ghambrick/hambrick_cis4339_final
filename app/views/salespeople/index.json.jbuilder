json.array!(@salespeople) do |salesperson|
  json.extract! salesperson, :id, :name, :phone, :salesmanager_id
  json.url salesperson_url(salesperson, format: :json)
end
