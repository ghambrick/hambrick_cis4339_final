json.array!(@salesmanagers) do |salesmanager|
  json.extract! salesmanager, :id, :name, :phone
  json.url salesmanager_url(salesmanager, format: :json)
end
