json.array!(@loans) do |loan|
  json.extract! loan, :id, :loan_number, :quote_id, :purprice, :downpayment, :term, :tax, :rate, :principle
  json.url loan_url(loan, format: :json)
end
