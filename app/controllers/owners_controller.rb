class OwnersController < ApplicationController

  def index
    @car_sum_cost = Car.sum_cost
    @car_sum_cost_sold = Car.sum_cost_sold
    @quote_sum_price = Quote.sum_price
    @quote_sum_price_sold = Quote.sum_price_sold
    @all_quotes_count  = Quote.count
    @quote_count_sold = Quote.count_sold
    @sales_tax_paid = tax
    @net_profit = netprofit
    @cost_car_available = cost_car_available
    @open_quote = open_quote

  end

def tax
  (@quote_sum_price_sold *0.043)
end

def netprofit
  (@quote_sum_price_sold - @quote_sum_price_sold / 1.1 )
end

def open_quote
    Quote.where("date_sold is null").count
end

  def cost_car_available
  @car_sum_cost-(@quote_sum_price_sold/1.1)
  end

end