class SalesmanagersController < ApplicationController
  before_action :set_salesmanager, only: [:show, :edit, :update, :destroy]

  # GET /salesmanagers
  # GET /salesmanagers.json
  def index
    @salesmanagers = Salesmanager.all
    @quote_sum_price = Quote.sum_price
    @quote_sum_price_sold = Quote.sum_price_sold
    @salespersons = Salesperson.all
  end

  # GET /salesmanagers/1
  # GET /salesmanagers/1.json
  def show
  end

  # GET /salesmanagers/new
  def new
    @salesmanager = Salesmanager.new
  end

  # GET /salesmanagers/1/edit
  def edit
  end

  # POST /salesmanagers
  # POST /salesmanagers.json
  def create
    @salesmanager = Salesmanager.new(salesmanager_params)

    respond_to do |format|
      if @salesmanager.save
        format.html { redirect_to @salesmanager, notice: 'Salesmanager was successfully created.' }
        format.json { render :show, status: :created, location: @salesmanager }
      else
        format.html { render :new }
        format.json { render json: @salesmanager.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /salesmanagers/1
  # PATCH/PUT /salesmanagers/1.json
  def update
    respond_to do |format|
      if @salesmanager.update(salesmanager_params)
        format.html { redirect_to @salesmanager, notice: 'Salesmanager was successfully updated.' }
        format.json { render :show, status: :ok, location: @salesmanager }
      else
        format.html { render :edit }
        format.json { render json: @salesmanager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /salesmanagers/1
  # DELETE /salesmanagers/1.json
  def destroy
    @salesmanager.destroy
    respond_to do |format|
      format.html { redirect_to salesmanagers_url, notice: 'Salesmanager was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_salesmanager
      @salesmanager = Salesmanager.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def salesmanager_params
      params.require(:salesmanager).permit(:name, :phone)
    end
end
