class LoansController < ApplicationController
  before_action :set_loan, only: [:show, :edit, :update, :destroy]

  # GET /loans
  # GET /loans.json
  def index
    @loans = Loan.all
    @loan_interest = Loan.interest
    @loan_rate = Loan.rate
    @loan_period = Loan.period
    @loan_principle = Loan.principle
    @loan_monthly_payment = Loan.monthly_payment
    @loan_total_amount_borrowed = Loan.total_amount_borrowed
    @loan_total_interest = Loan.total_interest
    @loan_tax = Loan.tax
    @loan_amount = Loan.amount
  end

  # GET /loans/1
  # GET /loans/1.json
  def show
   # @amortize_array = @loan.calculate_amortize_table
  end

  # GET /loans/new
  def new
   loan = Loan.new
  end

  # GET /loans/1/edit
  def edit
  end

  # POST /loans
  # POST /loans.json

  def create
    @loan_calculator = Loan.new(loan_calculator_params)
    respond_to do |format|
      if @loan_calculator.save
        format.html { redirect_to @loan, notice: 'Loan calculator was successfully created.' }
        format.json { render :show, status: :created, location: @loan }
      else
        format.html { render :new }
        format.json { render json: @loan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /loans/1
  # PATCH/PUT /loans/1.json
  def update
    respond_to do |format|
      if @loan.update(loan_params)
        format.html { redirect_to @loan_calculator, notice: 'Loan calculator was successfully updated.' }
        format.json { render :show, status: :ok, location: @loan }
      else
        format.html { render :edit }
        format.json { render json: @loan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /loans/1
  # DELETE /loans/1.json
  def destroy
    @loan.destroy
    respond_to do |format|
      format.html { redirect_to loan_url, notice: 'Loan calculator was successfully destroyed.' }
      format.json { head :no_content }
    end
end

  private
    # Use callbacks to share common setup or constraints between actions.
def set_loan
  @loan = Loan.find(params[:id])
end

    # Never trust parameters from the scary internet, only allow the white list through.

  def loan_params
    params.require(:loan).permit(:loan_number, :quote_id, :purprice, :downpayment, :term, :tax, :rate, :principle)
  end
end