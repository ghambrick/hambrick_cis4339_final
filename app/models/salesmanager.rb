class Salesmanager < ActiveRecord::Base
  has_many :salespersons

  def quote_by_salesperson
    Quote.select("sum(price).group(salesmamager_id)")
  end

  def quote_sum_price
    Quote.sum(:price, :conditions => "quote.date_sold NOT NULL")
  end


  def quote_sum_price_sold
    Quote.sum(:price, :conditions => "quote.date_sold IS NOT NULL")
  end


  def quote_count
    @quote_count = Quote.count
  end

end
