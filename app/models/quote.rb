class Quote < ActiveRecord::Base

  belongs_to :salesperson
  belongs_to :customer
  belongs_to :car


before_save :quote_check_sold


  def date_created_cannot_be_in_the_past
    errors.add(:date_created, "can't be in the past")if
        !date_created.blank? and date_created <Date.today
  end

  def self.sum_price
    Quote.where("date_sold IS NULL").sum(:price)
  end

  def self.sum_price_sold
    Quote.where("date_sold is not null").sum(:price)
  end

  def self.count_sold
    Quote.where("date_sold is not null").count(:price)
  end

  def self.quotes_by_salesperson
    Quote.sum(:price).group("salesperson_id")
  end

  def self.q_open
    where("date_sold is null")
  end

  def self.q_closed
    where("date_sold is not null")
  end

   def self.sum_salesperson_sold(salesperson_id)
       where("salesperson_id = ? AND date_sold is not null", salesperson_id ).sum(:price)
  end

  def quote_check_sold

    if unless self.date_sold.nil?
         car.update_column(:sold, true)
       end
    end
   end
end