class Loan < ActiveRecord::Base

  belongs_to :quote

def loan_term
 loan_term =36
end

def self.tax
   tax = 4.3/100
end

def self.amount
   amount = 50000
end

def self.principle
  principle = Loan.amount + (Loan.amount * tax)
end

def self.term
  term = 3
end

def self.period
  period = term*12
end

def self.rate
   rate = 11.5
end

def self.interest
  interest = (rate/100) /12
end

def self.monthly_payment
  monthly_payment = Loan.interest * Loan.principle * (1+Loan.interest)**Loan.period / (1+Loan.interest)**(Loan.period)-1
end

def self.total_amount_borrowed
    total_amount_borrowed = Loan.principle * (1+Loan.interest)**Loan.period
end

def self.total_interest
    total_interest = Loan.total_amount_borrowed - Loan.principle
end

def calc_total_interest
  total_interest = loan_amount - principle
end

def calc_amortize
  loan_amount = (loan_amount - monthly_pmt)
end

end