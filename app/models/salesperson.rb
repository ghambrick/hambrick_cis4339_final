class Salesperson < ActiveRecord::Base

  belongs_to :salesmanager
  has_many :quotes
  has_many :customers
end