class Car < ActiveRecord::Base

  belongs_to :quote

  validates :year, presence: true
  validates :make, presence: true
  validates :model, presence: true
  validates :vin, presence: true, uniqueness: true
  validates :color, presence: true

  def self.sum_cost
    Car.sum(:cost)
  end

  def self.sum_cost_sold
    Car.sum(:cost)
  end

  def self.car_available
   Car.where(sold:  false)
  end

  def self.car_sold
    where(sold:  true)
  end

  def self.search(search)
   where("make LIKE ? OR model LIKE ? OR vin LIKE ? OR color LIKE ?", "%#{search}%", "%#{search}%", "%#{search}%","%#{search}%")
    # where("make LIKE ?","%#{search}%")
  end

end
